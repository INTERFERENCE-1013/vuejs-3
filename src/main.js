import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import '@babel/polyfill'

import Blank from "./layouts/Blank.vue";
import Default from "./layouts/Default.vue";
import ErrorL from "./layouts/Error.vue";

Vue.component('default-layout', Default);
Vue.component('blank-layout', Blank);
Vue.component('error-layout', ErrorL);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
